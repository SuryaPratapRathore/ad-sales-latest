({
  getVideoContents: function(component) {
    var targets = [
      {'id': 0, 'name': 'Bundles', 'hasChildren': true, 'parentId': '0000','sub_category':'Bundles'},
      {'id': 1, 'name': 'Video content', 'hasChildren': true, 'parentId': '0000','sub_category':'Content'}
    ];
    component.set('v.targets', targets);
  }
});