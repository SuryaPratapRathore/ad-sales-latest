({
  getMidRollVideo: function(component) {
    var adunit = component.get('v.adunit');
    var ad_pod_positions = component.get('v.ad_pod_positions');
    var children = [];
    var parentIds = [adunit.id];
    if(adunit.parentId) parentIds.push(adunit.parentId);
    if(ad_pod_positions === 0) {
      children.push({
        id: 1,
        name: 'Position 1 in pod',
        index: 1,
        parentId : adunit.id,
        parentName: adunit.name,
        parentIds:parentIds
      });
    }

    for(var i = 1; i <= ad_pod_positions; i++) {
      children.push({
        id: i,
        name: 'Position ' + i + ' in pod',
        index: i,
        parentId : adunit.id,
        parentName: adunit.name,
        parentIds:parentIds
      });
    }
    children.push(
      {
        id: ad_pod_positions==0 ? 2 : ad_pod_positions+1,
        name: 'Last in pod',
        index: 100,
        parentId : adunit.id,
        parentName: adunit.name,
        parentIds:parentIds
      });
    component.set('v.targets', children);
    this.expandNodes(component);
  },
  expandNodes: function(component) {
    var flag = component.get('v.expand');
    component.set('v.expand', !flag);
  },
  fireEvent: function(component, action) {
    var adunit = component.get('v.adunit');
    if(adunit.name === 'Any mid-roll'){
      component.set('v.isMidRollIncluded',true);
    }
    var includeExcludeEvent = $A.get('e.c:TargetingEvent');
    includeExcludeEvent.setParams({
      'adunit': adunit,
      'action': action,
      'segments':component.get('v.segments'),
      'category': component.get('v.category')
    });
    includeExcludeEvent.fire();
  }
});