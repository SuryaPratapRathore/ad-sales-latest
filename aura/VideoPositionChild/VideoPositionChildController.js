({
  doInit: function(component, event, helper) {
    var targets = component.get('v.selectedMap');
    var adunit = component.get('v.adunit');
    if(targets) {
      var parentId = (adunit.parentId) ? adunit.parentId : '0000';
      var category = component.get('v.category');
      var items = targets[category];
      if(items && items[parentId]) {
        var adunits = items[parentId];
        for(var i = 0; i < adunits.length; i++) {
          if(adunits[i].adunit.id === adunit.id) {
            component.set('v.included', true);
            break;
          }
        }
      }
    }
  },
  getTargets: function(component, event, helper) {
    var targets = component.get('v.targets');
    if(targets && targets.length > 0)
      helper.expandNodes(component);
    else
      helper.getMidRollVideo(component);
  },
  collapseNodes: function(component, event, helper) {
    var flag = component.get('v.expand');
    component.set('v.expand', !flag);
  },
  loadMore: function(component, event, helper) {
    helper.fetchGeoTargets(component);
  },
  handleMenuSelect: function(component, event, helper) {
    var action = event.getParam('value');
    component.set('v.included', true);
    helper.fireEvent(component, action);
  },
  handleRemovedTarget: function(component, event, helper) {
    var segments = component.get('v.segments');
    var eventSegments = event.getParam('segments');
    if(eventSegments !== segments) return;

    var item = event.getParam('item');
    var current = component.get('v.adunit');

    if(item.adunit.id === current.id) {
      component.set('v.included', false);
      if(current.name === 'Any mid-roll'){
        component.set('v.isMidRollIncluded',false);
      }
    }
  },
  handleValueAction: function(component, event) {
    component.set('v.included', false);
  },
  midRollChangeHandler:function(component){
    if(component.get('v.isMidRoll')){
      var isMidRollIncluded = component.get('v.isMidRollIncluded');
      component.set('v.included',isMidRollIncluded);
    }
  }
});