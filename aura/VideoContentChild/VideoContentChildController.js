({
  doInit: function(component, event, helper) {
    var segments = component.get('v.segments');
    var targets = component.get('v.selectedMap');
    var adunit = component.get('v.adunit');
    var selectedSegments = component.get('v.selectedSegments');
    if(!(selectedSegments && selectedSegments.indexOf('Ad units') !== -1) && ($A.util.isEmpty(targets) || (!targets['Inventory'])) && $A.util.isEmpty(adunit.parentId) && !segments) {
      component.set('v.included', true);
      helper.fireSelectionEvent(component, 'include');
      return;
    }
    helper.makeTargetsSelected(component);
  },
  getRecordsFromDFP: function(component, event, helper) {
    var children = component.get('v.children');
    if(children.length === 0)
      helper.getAdUnitsFromDFP(component);
    else
      helper.expandChildren(component);
  },
  collapseNodes: function(component, event, helper) {
    component.set('v.expand', false);
  },
  handleMenuSelect: function(component, event, helper) {
    var action = event.getParam('value');
    if(action === 'include'){
      component.set('v.included', true);
      component.set('v.showInclude', true);
    }
    else{
      component.set('v.excluded', true);
      component.set('v.showExclude', true);
    }

    helper.fireSelectionEvent(component, action);
  },
  handleUnselection: function(component, event, helper) {
    var segments = component.get('v.segments');
    var eventSegments = event.getParam('segments');
    if(eventSegments !== segments) return;
    var parentCategory = event.getParam('parentCategory');
    var item = event.getParam('item');
    var targets = component.get('v.selectedMap');
    var adunit = component.get('v.adunit');
    if(item.adunit.id === adunit.id) {
      if(item.action === 'include') {
        component.set('v.included', false);
      } else {
        component.set('v.excluded', false);
      }
    }
    if(!targets || !targets[parentCategory]) {
      component.set('v.showInclude', false);
      component.set('v.showExclude', false);
    }

  },
  search: function(component,event,helper){
    helper.searchInventoryHandler(component);
  },
  loadMore: function(component, event, helper) {
    helper.getAdUnitsFromDFP(component);
  },
  handleValueAction: function(component, event, helper) {
    component.set('v.included', false);
    component.set('v.excluded', false);
  },
  segementsChangeHandler: function(component, event, helper) {
    var segments = component.get('v.segments');
    var include = component.get('v.included');
    var targets = component.get('v.selectedMap');
    var adunit = component.get('v.adunit');
    var selectedSegments = component.get('v.selectedSegments');

    if((!selectedSegments) || (selectedSegments && selectedSegments.indexOf('Ad units') === -1 && selectedSegments.indexOf('Placements') === -1)) {
      if(!targets['Inventory'] && adunit.name === 'Ad Units' && !segments && !include) {
        component.set('v.included', true);
        helper.fireSelectionEvent(component, 'include');
      }
    }
  }
});