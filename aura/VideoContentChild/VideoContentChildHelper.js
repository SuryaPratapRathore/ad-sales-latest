({
  getAdUnitsFromDFP: function(component) {
    var adunit = component.get('v.adunit');
    var offset = component.get('v.offset');
    var page_size = component.get('v.page_size');
    var searchTerm = component.get('v.searchTerm');
    var self = this;
    this.fetchData(component, 'getVideoContents', {
      'parentId': adunit.id,
      'offset': offset,
      'page_size': page_size,
      'searchTerm': (searchTerm && searchTerm.trim())?searchTerm.trim():null,
      'subCategory':component.get('v.sub_category')
    }, function(res) {
      if(res.status === 'OK') {
        if(res.targets.length === 0)
          component.set('v.error','No data found.');
        else
          component.set('v.error','');
        var targets = component.get('v.children');
        component.set('v.children', targets.concat(res.targets));
        var end_offset = component.get('v.end_offset');
        if(end_offset === 0) {
          end_offset = Math.ceil((parseInt(res.totalSize) / page_size));
          component.set('v.end_offset', end_offset);
        }
        if(offset < end_offset)
          component.set('v.offset', ++offset);
        self.expandChildren(component);
      }else{
        component.set('v.error',res.msg);
      }
    });
  },
  searchInventoryHandler: function(component) {
    var term = component.get('v.searchTerm');
    var self = this;
    if(term && term.trim()) {
      component.set('v.children',[]);
      component.set('v.searchEnabled',true);
      component.set('v.offset',0);
      component.set('v.end_offset',0);
      clearTimeout(self.timeout);
      self.timeout = setTimeout($A.getCallback(function() {
        self.getAdUnitsFromDFP(component);
      }), 500);
    }else if(component.get('v.searchEnabled')){
      component.set('v.children',[]);
      component.set('v.offset',0);
      component.set('v.end_offset',0);
      component.set('v.searchEnabled',false);
      clearTimeout(self.timeout);
      self.timeout = setTimeout($A.getCallback(function() {
        self.getAdUnitsFromDFP(component);
      }), 500);
    }
  },
  fireSelectionEvent: function(component, action) {
    var compEvent = $A.get('e.c:TargetingEvent');
    compEvent.setParams({
      'adunit': component.get('v.adunit'),
      'action': action,
      'segments':component.get('v.segments'),
      'category': component.get('v.category'),
      'sub_category': component.get('v.sub_category')
    });
    compEvent.fire();
  },
  expandChildren: function(component) {
    component.set('v.expand', true);
  },
  isEmpty: function(obj) {
    for(var key in obj) {
      if(obj.hasOwnProperty(key))
        return false;
    }
    return true;
  },
  makeTargetsSelected: function(component){
    try {
      var targets = component.get('v.selectedMap');
      var adunit = component.get('v.adunit');

      if(targets) {
        var parentId = (adunit.parentId) ? adunit.parentId : '0000';
        var category = component.get('v.category');
        var subCategory = component.get('v.sub_category');
        var rootTargets = targets[category];
        if(rootTargets) {
          var items = rootTargets[subCategory];
          if(items && items[parentId]) {
            var adunits = items[parentId];
            for(var i = 0; i < adunits.length; i++) {
              if(adunits[i].adunit.id === adunit.id) {
                if(adunits[i].action === 'include')
                  component.set('v.included', true);
                else
                  component.set('v.excluded', true);
                break;
              }
            }
          }/*else if(component.get('v.lineItems') > 0){
            component.set('v.customizable',false);
          }*/
        }
      }
    } catch(e) {console.log(e.message);}
  }
});