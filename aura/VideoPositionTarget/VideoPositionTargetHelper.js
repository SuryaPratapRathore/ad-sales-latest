({
  getTargets: function(component) {
    this.fetchData(component, 'getVideoPositions', {}, function(res) {
      if(res.status === 'OK') {
        component.set('v.ad_pod_positions', res.ad_pod_positions);
        component.set('v.targets', res.targets);
      }
      else{
        component.set('v.error', res.msg);
      }
      component.set('v.showSpinner', false);
    },function(error){
      component.set('v.showSpinner', false);
      component.set('v.error', error);
    });
  }
});