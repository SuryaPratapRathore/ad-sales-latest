@isTest
public class ContentService_Test {
    static testMethod void testContentService() { 
        Test.startTest(); 
        Test.setMock(WebServiceMock.class, new WebServiceMockContentService());
        ContentService.getContentByStatementAndCustomTargetingValueResponse_element objgetContentResponse = new ContentService.getContentByStatementAndCustomTargetingValueResponse_element();
        ContentService.PublisherQueryLanguageSyntaxError objPublisherQueryLanguageSyntaxError = new ContentService.PublisherQueryLanguageSyntaxError();
        ContentService.DaiIngestError objDaiIngestError = new ContentService.DaiIngestError();
        ContentService.NotNullError objNotNullError = new ContentService.NotNullError();
        ContentService.DateTime_x objDateTime_x = new ContentService.DateTime_x();
        ContentService.InvalidUrlError objInvalidUrlError = new ContentService.InvalidUrlError();
        ContentService.QuotaError objQuotaError = new ContentService.QuotaError();
        ContentService.String_ValueMapEntry objString_ValueMapEntry = new ContentService.String_ValueMapEntry();
        ContentService.ObjectValue objObjectValue = new ContentService.ObjectValue();
        ContentService.RequiredNumberError objRequiredNumberError = new ContentService.RequiredNumberError();
        ContentService.NumberValue objNumberValue = new ContentService.NumberValue();
        ContentService.getContentByStatementResponse_element objgetContentByStatementResponse_element = new ContentService.getContentByStatementResponse_element();
        ContentService.ApiVersionError objApiVersionError = new ContentService.ApiVersionError();
        ContentService.DateValue objDateValue = new ContentService.DateValue();
        ContentService.BooleanValue objBooleanValue = new ContentService.BooleanValue();
        ContentService.RequiredCollectionError objRequiredCollectionError = new ContentService.RequiredCollectionError();
        ContentService.ParseError objParseError = new ContentService.ParseError();
        ContentService.StringLengthError objStringLengthError = new ContentService.StringLengthError();
        ContentService.CollectionSizeError objCollectionSizeError = new ContentService.CollectionSizeError();
        ContentService.SoapResponseHeader objSoapResponseHeader = new ContentService.SoapResponseHeader();
        ContentService.getContentByStatementAndCustomTargetingValue_element objgetContent = new ContentService.getContentByStatementAndCustomTargetingValue_element();
        ContentService.AuthenticationError objAuthenticationError = new ContentService.AuthenticationError();
        ContentService.CmsContent objCmsContent = new ContentService.CmsContent();
        ContentService.DateTimeValue objDateTimeValue = new ContentService.DateTimeValue();
        ContentService.PermissionError objPermissionError = new ContentService.PermissionError();
        ContentService.ServerError objServerError = new ContentService.ServerError();
        ContentService.Statement objStatement = new ContentService.Statement();
        ContentService.StringFormatError objStringFormatError = new ContentService.StringFormatError();
        ContentService.FieldPathElement objFieldPathElement = new ContentService.FieldPathElement();
        ContentService.RequiredError objRequiredError = new ContentService.RequiredError();
        ContentService.Date_x objDate_x = new ContentService.Date_x();
        ContentService.PublisherQueryLanguageContextError objPublisherQueryLanguageContextError = new ContentService.PublisherQueryLanguageContextError();
        ContentService.getContentByStatement_element objgetContentByStatement_element = new ContentService.getContentByStatement_element();
        ContentService.InternalApiError objInternalApiError = new ContentService.InternalApiError();
        ContentService.SoapRequestHeader objSoapRequestHeader = new ContentService.SoapRequestHeader();
        ContentService.ApplicationException objApplicationException = new ContentService.ApplicationException();
        ContentService.StatementError objStatementError = new ContentService.StatementError();
        ContentService.Content objContent = new ContentService.Content();
        ContentService.TextValue objTextValue = new ContentService.TextValue();
        ContentService.Value objValue = new ContentService.Value();
        ContentService.TypeError objTypeError = new ContentService.TypeError();
        ContentService.ApiError objApiError = new ContentService.ApiError();
        ContentService.CommonError objCommonError = new ContentService.CommonError();
        ContentService.ContentPage objContentPage = new ContentService.ContentPage();
        ContentService.FeatureError objFeatureError = new ContentService.FeatureError();
        ContentService.SetValue objSetValue = new ContentService.SetValue();
        ContentService.ApiException objApiException = new ContentService.ApiException();
        ContentService.ContentServiceInterfacePort objContentServiceInterfacePort = new ContentService.ContentServiceInterfacePort();
		objContentServiceInterfacePort.getContentByStatement(objStatement);
        objContentServiceInterfacePort.getContentByStatementAndCustomTargetingValue(objStatement, 1234567);
        
        Test.stopTest();
    }
}