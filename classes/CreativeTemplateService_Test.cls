@isTest
public class CreativeTemplateService_Test {

	@isTest public static void testCreativeData(){
    Test.startTest();
    Test.setMock(WebServiceMock.class, new WebServiceMockInventoryService());
		CreativeTemplateService.LongCreativeTemplateVariable tempVariable = new CreativeTemplateService.LongCreativeTemplateVariable();
		tempVariable.defaultValue = 1234;

		List<CreativeTemplateService.String_ValueMapEntry> lstValMap = new List<CreativeTemplateService.String_ValueMapEntry>();

		CreativeTemplateService.String_ValueMapEntry valMap = new CreativeTemplateService.String_ValueMapEntry();
    valMap.key = 'key00';
    valMap.value = new CreativeTemplateService.Value();
    lstValMap.add(valMap);

		CreativeTemplateService.Statement stm = new CreativeTemplateService.Statement();
		stm.query = 'where id! = 0';
		stm.values = lstValMap;

		CreativeTemplateService.getCreativeTemplatesByStatement_element creativeStm = new CreativeTemplateService.getCreativeTemplatesByStatement_element();
    creativeStm.filterStatement = stm;

    CreativeTemplateService.NotNullError nNError = new CreativeTemplateService.NotNullError();
    nNError.reason = 'Not null Error';
    CreativeTemplateService.Date_x dtx = new CreativeTemplateService.Date_x();
    CreativeTemplateService.DateTime_x dt = new CreativeTemplateService.DateTime_x();
    dt.date_x = dtx;
    dt.hour = 10;
    dt.minute = 23;
    dt.second = 30;
    CreativeTemplateService.AssetCreativeTemplateVariable asset = new CreativeTemplateService.AssetCreativeTemplateVariable();
    CreativeTemplateService.NullError nError = new CreativeTemplateService.NullError();
    CreativeTemplateService.QuotaError qError = new CreativeTemplateService.QuotaError();
    CreativeTemplateService.ObjectValue obVal = new CreativeTemplateService.ObjectValue();
    CreativeTemplateService.RequiredNumberError reqNumError = new CreativeTemplateService.RequiredNumberError();
    CreativeTemplateService.ApiVersionError apiVerError = new CreativeTemplateService.ApiVersionError();
    CreativeTemplateService.BooleanValue bVal = new CreativeTemplateService.BooleanValue();
    CreativeTemplateService.CreativeTemplateOperationError opError = new CreativeTemplateService.CreativeTemplateOperationError();
    CreativeTemplateService.RequiredCollectionError reqCError = new CreativeTemplateService.RequiredCollectionError();

    CreativeTemplateService.StringLengthError strLenError = new CreativeTemplateService.StringLengthError();
    CreativeTemplateService.CollectionSizeError cSizeError = new CreativeTemplateService.CollectionSizeError();
    CreativeTemplateService.SoapResponseHeader soapResHeader = new CreativeTemplateService.SoapResponseHeader();
    CreativeTemplateService.UniqueError unqError = new CreativeTemplateService.UniqueError();
    CreativeTemplateService.AuthenticationError authError = new CreativeTemplateService.AuthenticationError();
    CreativeTemplateService.DateTimeValue dtTimeVal = new CreativeTemplateService.DateTimeValue();
    dtTimeVal.value = dt;
    CreativeTemplateService.PermissionError perError = new CreativeTemplateService.PermissionError();
    CreativeTemplateService.ServerError serError = new CreativeTemplateService.ServerError();
    CreativeTemplateService.CreativeTemplate creTemplate = new CreativeTemplateService.CreativeTemplate();
    CreativeTemplateService.getCreativeTemplatesByStatementResponse_element creTemplateStmRes = new CreativeTemplateService.getCreativeTemplatesByStatementResponse_element();
    CreativeTemplateService.FieldPathElement flPathEle = new CreativeTemplateService.FieldPathElement();
    CreativeTemplateService.PublisherQueryLanguageContextError pubQLcnt = new CreativeTemplateService.PublisherQueryLanguageContextError();
    CreativeTemplateService.InternalApiError intApiError = new CreativeTemplateService.InternalApiError();
    CreativeTemplateService.SoapRequestHeader soapReqHeader = new CreativeTemplateService.SoapRequestHeader();
    CreativeTemplateService.TextValue txtVal = new CreativeTemplateService.TextValue();
    CreativeTemplateService.ApiError apiError = new CreativeTemplateService.ApiError();
    CreativeTemplateService.FeatureError featError = new CreativeTemplateService.FeatureError();
    CreativeTemplateService.StringCreativeTemplateVariable strTempVar = new CreativeTemplateService.StringCreativeTemplateVariable();
    CreativeTemplateService.PublisherQueryLanguageSyntaxError pqlSyntaxError = new CreativeTemplateService.PublisherQueryLanguageSyntaxError();
    CreativeTemplateService.InvalidUrlError ildUriError = new CreativeTemplateService.InvalidUrlError();
    CreativeTemplateService.ListStringCreativeTemplateVariable_VariableChoice tempVarCh = new CreativeTemplateService.ListStringCreativeTemplateVariable_VariableChoice();
    CreativeTemplateService.NumberValue numVal = new CreativeTemplateService.NumberValue();
    CreativeTemplateService.DateValue dtVal = new CreativeTemplateService.DateValue();
    CreativeTemplateService.RangeError rngError = new CreativeTemplateService.RangeError();
    CreativeTemplateService.ParseError parError = new CreativeTemplateService.ParseError();
    CreativeTemplateService.CreativeTemplatePage creatTempPage = new CreativeTemplateService.CreativeTemplatePage();
    CreativeTemplateService.CreativeTemplateError creatTempError = new CreativeTemplateService.CreativeTemplateError();
    CreativeTemplateService.UrlCreativeTemplateVariable urlCreatTempVal = new CreativeTemplateService.UrlCreativeTemplateVariable();
    CreativeTemplateService.StringFormatError strFormatError = new CreativeTemplateService.StringFormatError();
    CreativeTemplateService.RequiredError reqError = new CreativeTemplateService.RequiredError();

    CreativeTemplateService.ListStringCreativeTemplateVariable lstCreatTempVar = new CreativeTemplateService.ListStringCreativeTemplateVariable();
    CreativeTemplateService.ApplicationException appExc = new CreativeTemplateService.ApplicationException();
    CreativeTemplateService.StatementError stmError = new CreativeTemplateService.StatementError();
    CreativeTemplateService.CommonError cmError = new CreativeTemplateService.CommonError();
    CreativeTemplateService.SetValue stVal = new CreativeTemplateService.SetValue();
    CreativeTemplateService.ApiException apiExc = new CreativeTemplateService.ApiException();
    CreativeTemplateService.CreativeTemplateServiceInterfacePort service = new CreativeTemplateService.CreativeTemplateServiceInterfacePort();

    service.getCreativeTemplatesByStatement(stm);
    Test.stopTest();

	}
}