@IsTest
global class WebServiceMockContentService implements WebServiceMock {
	global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
        if(request instanceof ContentService.getContentByStatementAndCustomTargetingValue_element)
            response.put('response_x', new ContentService.getContentByStatementAndCustomTargetingValueResponse_element());
        else if(request instanceof ContentService.getContentByStatement_element)
            response.put('response_x', new ContentService.getContentByStatementResponse_element());
        return;
    }
}